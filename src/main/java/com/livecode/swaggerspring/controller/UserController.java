package com.livecode.swaggerspring.controller;


import com.livecode.swaggerspring.model.User;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@Tag(name = "User")
public class UserController {

    @RequestMapping(method = RequestMethod.GET, path = "/user")
    @Operation(summary = "Get user", responses = {
            @ApiResponse(description = "Get user success", responseCode = "200",
                    content = @Content(mediaType = "application/json",schema = @Schema(implementation = User.class))),
            @ApiResponse(description = "User not found",responseCode = "409",content = @Content)
    })
    public ResponseEntity<User> getUser(String id) {
        if ("1".equals(id)) {
            User user = new User();
            user.setId("1");
            user.setName("Live Code");
            return ResponseEntity.ok(user);
        } else {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Not found");
        }
    }

    @RequestMapping(method = RequestMethod.POST, path = "/user")
    public ResponseEntity<Void> updateUser(User user) {
        // Update user logic here ...

        return ResponseEntity.ok().build();
    }
}
